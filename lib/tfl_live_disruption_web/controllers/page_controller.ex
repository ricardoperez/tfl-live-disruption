defmodule Tfl.LiveDisruptionWeb.PageController do
  use Tfl.LiveDisruptionWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
