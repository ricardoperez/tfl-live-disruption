defmodule Tfl.LiveDisruptionWeb.DisruptionChannel do
  use Tfl.LiveDisruptionWeb, :channel
  alias Tfl.LiveDisruption.Storage

  def join("disruption", _payload, socket) do
    send(self(), :all)
    {:ok, socket}
  end

  def handle_info(:all, socket) do
    disruptions =
      Storage.fetch_all()
      |> Enum.map(fn {_k, disruption} -> disruption end)

    broadcast!(socket, "all", %{disruptions: disruptions})
    {:noreply, socket}
  end
end
