defmodule Tfl.LiveDisruption.Parsers.Disruption do
  import SweetXml

  @coord_starts_wo_number_re ~r/^(\D)?\.(.*)$/

  def get_id(disruption) do
    xpath(disruption, ~x"@id"i)
  end

  def status(disruption) do
    xpath(disruption, ~x"status/text()"s)
  end

  def category(disruption) do
    xpath(disruption, ~x"category/text()"s)
  end

  def updates(disruption) do
    xpath(disruption, ~x"currentUpdate/text()"s)
  end

  def comments(disruption) do
    xpath(disruption, ~x"comments/text()"s)
  end

  def location(disruption) do
    xpath(disruption, ~x"location/text()"s)
  end

  def severity(disruption) do
    xpath(disruption, ~x"severity/text()"s)
  end

  def start_time(disruption) do
    xpath(disruption, ~x"startTime/text()"s)
  end

  def end_time(disruption) do
    xpath(disruption, ~x"endTime/text()"s)
  end

  def coordinates(disruption) do
    disruption
    |> xpath(~x"CauseArea/DisplayPoint/Point/coordinatesLL/text()"s)
    |> parse_coordinates()
  end

  defp parse_coordinates(coordinates) when is_binary(coordinates) do
    coordinates
    |> String.split(",")
    |> parse_coordinates()
  end

  defp parse_coordinates([longitude, latitude]) do
    [parse_coordinate(latitude), parse_coordinate(longitude)]
  end

  defp parse_coordinate(coordinate) do
    @coord_starts_wo_number_re
    # fix coordinates like this -.3532
    |> Regex.replace(coordinate, "\\g{1}0.\\g{2}")
    |> Float.parse()
    |> elem(0)
  end
end
