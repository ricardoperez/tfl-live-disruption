defmodule Tfl.LiveDisruption.Parsers.Feed do
  alias Tfl.LiveDisruption.Models.Disruption
  import SweetXml

  def list_disruptions(parsed_xml) do
    parsed_xml
    |> xpath(~x"/Root/Disruptions/Disruption"l)
    |> Enum.map(&Disruption.from_disruption_xml/1)
  end
end
