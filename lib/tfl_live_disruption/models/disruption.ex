defmodule Tfl.LiveDisruption.Models.Disruption do
  import Tfl.LiveDisruption.Parsers.Disruption

  defstruct [
    :id,
    :status,
    :category,
    :location,
    :severity,
    :start_time,
    :end_time,
    :comments,
    :updates,
    :coordinates
  ]

  def from_disruption_xml(disruption) do
    %__MODULE__{
      id: get_id(disruption),
      status: status(disruption),
      category: category(disruption),
      location: location(disruption),
      severity: severity(disruption),
      coordinates: coordinates(disruption),
      start_time: start_time(disruption),
      comments: comments(disruption),
      updates: updates(disruption),
      end_time: end_time(disruption)
    }
  end
end
