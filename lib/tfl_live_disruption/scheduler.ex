defmodule Tfl.LiveDisruption.Scheduler do
  use GenServer
  alias Tfl.LiveDisruption.Client
  alias Tfl.LiveDisruption.FetcherServer
  alias Tfl.LiveDisruption.Parsers.Feed
  alias Tfl.LiveDisruption.Storage

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    work()
    schedule_work()
    {:ok, state}
  end

  def handle_info(:fetch, state) do
    work()
    schedule_work()
    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :fetch, 20 * 60 * 1000)
  end

  defp work() do
    if Mix.env() != :test do
      IO.ANSI.Docs.print_heading("Start fetching data from Tfl TIMS feed!")
      {:ok, fetcher_server} =  FetcherServer.start_link()
      disruptions = Client.get_disruptions_xml(fetcher_server) # 
    end
  end
end
