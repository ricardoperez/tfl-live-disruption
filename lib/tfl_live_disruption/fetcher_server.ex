defmodule Tfl.LiveDisruption.FetcherServer do
  use GenServer
  alias Tfl.LiveDisruption.Parsers.Feed
  alias Tfl.LiveDisruption.Storage

  def start_link() do
    GenServer.start_link(__MODULE__, "")
  end

  def init(args) do
    {:ok, args}
  end

  def handle_info(%HTTPoison.AsyncChunk{chunk: chunk}, state) do
    {:noreply, state <> chunk}
  end

  def handle_info(%HTTPoison.AsyncEnd{}, state) do
    IO.ANSI.Docs.print_heading("Finish Fetching data from Tfl.")
    IO.ANSI.Docs.print_heading("Caching data...")
    state 
    |> Feed.list_disruptions()
    |> Enum.each(fn disruption ->
      Storage.cache(disruption, disruption.id)
    end)

    IO.ANSI.Docs.print_heading("Work done!")
    {:noreply, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end
end