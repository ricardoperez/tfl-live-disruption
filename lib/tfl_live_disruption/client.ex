defmodule Tfl.LiveDisruption.Client do
  @tims_feed_url "https://data.tfl.gov.uk/tfl/syndication/feeds/tims_feed.xml"

  @spec get_disruptions_xml(pid) :: HTTPoison.AsyncResponse.t()
  def get_disruptions_xml(fetcher_pid) do
    @tims_feed_url
    |> HTTPoison.get!(%{}, stream_to: fetcher_pid)
  end
end
