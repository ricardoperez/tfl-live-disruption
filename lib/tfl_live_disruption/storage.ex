defmodule Tfl.LiveDisruption.Storage do
  @table_name :disruptions

  def cache(data, key) do
    set_ets_table()
    :ets.insert(@table_name, {key, data})
  end

  def fetch(key) do
    set_ets_table()
    fetch(:ets.lookup(@table_name, key))
  end

  def fetch_all() do
    :ets.select(@table_name, [{:"$1", [], [:"$1"]}])
  end

  defp set_ets_table() do
    if :ets.info(@table_name) == :undefined do
      :ets.new(@table_name, [:named_table])
    end
  end
end
