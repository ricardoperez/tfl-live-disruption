import socket from "./socket"
import React from 'react';
import ReactDOM from 'react-dom';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';

class SimpleExample extends React.Component {
  constructor() {
    super()
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 13,
      markers: []
    }
  }

  componentDidMount() {
    this.loadDisruptions();
  }
 
  loadDisruptions () {
    let channel = socket.channel("disruption", {});
    channel.join();
    channel.on('all', resp => {
      this.setState({markers: resp.disruptions})
    });
  }

  renderMarkers() {
    return (
      this.state.markers.map((disruption, idx) => {
        var updatesMsg;
        var commentsMsg;
        var startTime;
        var endTime;

        if (disruption.updates) {
          updatesMsg = <p>💬 {disruption.updates}</p>;
        } 

        if (disruption.comments) {
          commentsMsg = <p>💬 {disruption.comments}</p>;
        }

        if (disruption.start_time) {
          startTime = <p>📅 Started at: {disruption.start_time}</p>
        }

        if (disruption.end_time) {
          endTime = <p>📆  Possibly finishes at: {disruption.end_time}</p>
        }

        return (<Marker key={`marker-${idx}`} position={disruption.coordinates}>
          <Popup key={`popup-${idx}`}>
          <p>📍 {disruption.location}</p>
          <p>💥 {disruption.category} </p>
          <p>📝 {disruption.status}</p>
          <p>Severity: {disruption.severity}</p>
          {commentsMsg}
          {updatesMsg}
          {startTime}
          {endTime}
          </Popup>
        </Marker>);
      })
      );
  }

  render() {
    const position = [this.state.lat, this.state.lng];
    return (
      <Map center={position} zoom={this.state.zoom}>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {this.renderMarkers()}
      </Map>
    );
  }
}

ReactDOM.render(<SimpleExample />, document.getElementById('leaf-map'))
