defmodule Tfl.LiveDisruption.XMLHelper do
  @full_xml_feed_path "test/support/xmls/tims_feed.xml"
  @small_xml_feed_path "test/support/xmls/small_feed.xml"
  @single_disruption_path "test/support/xmls/single_disruption.xml"

  def load_full_feed_xml do
    File.read!(@full_xml_feed_path)
  end

  def load_small_feed_xml do
    File.read!(@small_xml_feed_path)
  end

  def load_single_disruption_xml do
    File.read!(@single_disruption_path)
  end
end
