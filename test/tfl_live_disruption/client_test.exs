defmodule Tfl.LiveDisruption.ClientTest do
  use Tfl.LiveDisruptionWeb.ConnCase, async: true
  alias Tfl.LiveDisruption.Client
  alias Tfl.LiveDisruption.FetcherServer

  describe ".get_disruption_xml" do
    setup do
      :meck.expect(HTTPoison, :get!, fn _, _, _ -> %HTTPoison.AsyncResponse{} end)
    end

    test "returns encode xml" do
      {:ok, fetcher_server} =  FetcherServer.start_link()

      assert %HTTPoison.AsyncResponse{} = Client.get_disruptions_xml(fetcher_server)
   end
  end
end
