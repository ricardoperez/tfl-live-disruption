defmodule Tfl.LiveDisruption.FeedTest do
  use Tfl.LiveDisruptionWeb.ConnCase, async: true
  alias Tfl.LiveDisruption.Parsers.Feed
  alias Tfl.LiveDisruption.Models.Disruption

  @xml load_small_feed_xml()
  @parsed_xml @xml
              |> :binary.bin_to_list()
              |> :xmerl_scan.string()
              |> elem(0)

  describe ".list_disruptions/1" do
    test "returns list of Disruption structs" do
      assert [%Disruption{} | _] = Feed.list_disruptions(@parsed_xml)
    end

    test "returns 5 disruptions" do
      assert Enum.count(Feed.list_disruptions(@parsed_xml)) == 5
    end

    test "check last Disruption struct" do
      expected_disruption = %Disruption{
        id: 192_779,
        status: "Scheduled",
        severity: "Moderate",
        category: "Special and Planned Events",
        start_time: "2018-09-29T12:15:00Z",
        end_time: "2018-09-29T17:00:00Z",
        location: "Arsenal Fc, Emirates Stadium () (Islington)",
        comments:
          "Arsenal FC, Emirates Stadium - Arsenal v Watford, kick-off 1500hrs. Expect large numbers of pedestrian traffic in the area.",
        updates: "",
        coordinates: [51.554957, -0.108696]
      }

      assert expected_disruption == List.last(Feed.list_disruptions(@parsed_xml))
    end
  end
end
