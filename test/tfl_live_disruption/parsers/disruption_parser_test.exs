defmodule Tfl.LiveDisruption.Parsers.DisruptionTest do
  use Tfl.LiveDisruptionWeb.ConnCase, async: true
  alias Tfl.LiveDisruption.Parsers.Disruption
  import SweetXml

  @xml load_single_disruption_xml()
  @disruption xpath(@xml, ~x"/Root/Disruptions/Disruption")

  describe "parse disruption" do
    test "show id" do
      assert Disruption.get_id(@disruption) == 196_532
    end

    test "show status" do
      assert Disruption.status(@disruption) == "Active"
    end

    test "show category" do
      assert Disruption.category(@disruption) == "Traffic Incidents"
    end

    test "show comments" do
      assert Disruption.comments(@disruption) ==
               "[A40] Greenford Flyover (A40) (UB6) (Eastbound) - Lane three (of three) is currently blocked following a collision."
    end

    test "show updates" do
      assert Disruption.updates(@disruption) ==
               "Eastbound traffic is slow and congested on the approach from Polish War Memorial and there are additional delays along the A312 Church Road approaching Target Roundabout. Westbound A40 traffic is slowing due to onlookers. Seek other routes."
    end

    test "show location" do
      assert Disruption.location(@disruption) == "[A40] Greenford Flyover (A40) (UB6) (Ealing)"
    end

    test "show coordinates" do
      assert Disruption.coordinates(@disruption) == [51.537491, -0.353498]
    end

    test "show severity" do
      assert Disruption.severity(@disruption) == "Serious"
    end

    test "show start time" do
      assert Disruption.start_time(@disruption) == "2018-08-04T16:36:00Z"
    end

    test "do not show end time when tag is empty" do
      assert Disruption.end_time(@disruption) == ""
    end
  end
end
