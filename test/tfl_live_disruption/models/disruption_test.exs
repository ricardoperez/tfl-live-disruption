defmodule Tfl.LiveDisruption.Models.DisruptionTest do
  use Tfl.LiveDisruptionWeb.ConnCase, async: true
  alias Tfl.LiveDisruption.Models.Disruption
  import SweetXml

  @xml load_single_disruption_xml()
  @disruption xpath(@xml, ~x"/Root/Disruptions/Disruption")

  describe "from_disruption_xml/1" do
    test "returns Disruption Struct" do
      expected_disruption = %Disruption{
        id: 196_532,
        category: "Traffic Incidents",
        coordinates: [51.537491, -0.353498],
        end_time: "",
        location: "[A40] Greenford Flyover (A40) (UB6) (Ealing)",
        severity: "Serious",
        start_time: "2018-08-04T16:36:00Z",
        comments:
          "[A40] Greenford Flyover (A40) (UB6) (Eastbound) - Lane three (of three) is currently blocked following a collision.",
        updates:
          "Eastbound traffic is slow and congested on the approach from Polish War Memorial and there are additional delays along the A312 Church Road approaching Target Roundabout. Westbound A40 traffic is slowing due to onlookers. Seek other routes.",
        status: "Active"
      }

      assert Disruption.from_disruption_xml(@disruption) == expected_disruption
    end
  end
end
