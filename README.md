# TflLiveDisruption

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && yarn install`
  * Start Phoenix endpoint with `mix phx.server`

Or you can run with docker:

```
$ docker run --rm -it -v "$PWD":/app -p 4000:4000 -w /app elixir:1.7.1-alpine bin/run_with_docker.sh
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Running tests:

```
mix test
```

## Intructions

The first time when loading the project could be slow , or not show any markers on the mains page. The disruptions is being fetched and cached in background.

* Fetching/Downloading data using `HTTPoisong.get!/1`
* Fetching and Caching data in bg when app starts, see: `lib\tfl_live_disruption\scheduler.ex` .
* The frontend is retrieving data using phoenix websockets.

### Wish to have:

* Smater way to fetch data
* Persist disruptions on Database with GIS (and delete automatically).
* Load only necessary pins/markes according with map region set.
* Differents collors for differents types of incident/status/severity.
